# MacOS

MacOS und Linux haben sehr viel mehr gemeinsam, als es zunächsten Anschein haben mag. Tatsächlich ist MacOS, genauso wie Linux, ein Unixähnliches System. Beide haben also quasi den selben idellen Vorfahren. Um das volle Potential von MacOS zu nutzen, brauchen wir nur einen Paketmanager der uns Erlaubt Linux-Software für Mac einzurichten. Dieser Paketmanager heißt homebrew und ermöglicht uns danach alles andere zu installieren.

## Homebrew
Um den Paketmanager homebrew zu installieren, folge den Anweisungen auf der Seite [https://brew.sh/de/](https://brew.sh/de/) und kopiere den Befehl von dort in ein Terminal.

## VSCodium
Führe folgendes in einem Terminal aus:
```
brew install --cask vscodium
```

## Python
Führe folgendes in einem Terminal aus:
```
brew install python3
```

!!! note "Tipp"
    Um Python auf der Kommandozeile komfortabler zu benutzen, kannst du zusätzlich ipython installieren, dass eine etwas schickere Oberfläche auf der Kommandozeile bietet:

    ```
    brew install ipython
    ```


## LaTeX installieren (optional)

!!! warning
    Diese Anleitung musst du vermutlich nicht befolgen. Falls du sie befolgen musst, dann erst in der Einheit zu LaTeX.

Da das Dokumentensystem LaTeX sehr groß ist, empfehlen wir diese Anleitung nur zu befolgen, wenn du nicht

1. [https://sharelatex.gwdg.de/](https://sharelatex.gwdg.de/) nutzen kannst, weil du z.B. deine Logindaten noch nicht hast
2. [https://de.overleaf.com/](https://de.overleaf.com/) nicht nutzen möchtest, z.B. aus Datenschutzgründen

Gib in dein Terminal folgendes ein:
```
brew install --cask mactex
```

## Java

!!! warning
    Dieser Abschnitt ist erst für die Einheit zu Java relevant.

Vor der Installation sollte geprüft werden, ob auf dem System bereits Java vorhanden ist. Dazu sollten folgende Kommandos in einem Terminal ausgeführt werden. Keine Sorge - da darf eine Fehlermeldung angezeigt werden, allerdings nicht, dass die Kommandos nicht gefunden werden:

- `java`
- `javac`

Sollte einer der beiden Befehle nicht bekannt sein / gefunden werden, dann führe folgendes in einem Terminal aus:
```
brew install openjdk@11
```

Zusätzlich kann es nötig sein eine Verknüpfung (symbolischen Link) anzulegen, damit das System java erkennt:
```
sudo ln -sfn /usr/local/opt/openjdk@11/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk-11.jdk
```

Sobald du Java installiert hast, kannst du [unserer Anleitung zum testen folgen](/informatik-vorkurs-anleitungen/java-testen/).

# Zusätzliche Empfehlungen
Das Standard-Terminal unter MacOS ist tauglich aber nicht besonders mächtig. Stattdessen kannst du [iTerm2](https://iterm2.com/) installieren.