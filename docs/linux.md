# Linux
## Start

Es gibt viele Varianten von Linux (Distributionen). Wir konzentrieren uns hier auf die Linux-Distributionen, die auf Debian verwenden und den Paketmanager `apt` mitbringen. Das umfasst insbesondere das weit verbreitete und einstiegsfreundliche **Ubuntu**. Unter anderen Linux-Distributionen können Dinge leicht abweichen.

Im folgenden findest du alle Befehle, wie sie unter Ubuntu funktionieren.

## VSCodium

Folge den Anweisungen auf [https://vscodium.com/](https://vscodium.com/). Für Ubuntu befinden sich die korrekten Instruktionen unter [Install on Debian / Ubuntu (deb package):](https://vscodium.com/#install-on-debian-ubuntu-deb-package).

!!! info
    **Ein weiterführender Hinweis:**
    Wir raten dazu nicht snap zu verwenden, sondern der Sektion [Use a Package Manager (deb/rpm, provided by VSCodium related repository)](https://vscodium.com/#use-a-package-manager-deb-rpm-provided-by-vscodium-related-repos) zu folgen.

Es ist zwar technisch möglich in VSCodium [den offiziellen Marktplatz für Erweiterungen](https://www.flypenguin.de/2023/02/26/use-vscodium-with-microsofts-proprietary-marketplace/) zu aktivieren, allerdings verstößt dies gegen Microsofts Lizenzbedingungen.

## Python installieren

Hura! Du musst nichts tun, denn python ist in Ubuntu schon vorinstalliert.

Mehr findest du unter: [https://wiki.ubuntuusers.de/Python/](https://wiki.ubuntuusers.de/Python/)

!!! note "Tipp"
    Um Python auf der Kommandozeile komfortabler zu benutzen, kannst du zusätzlich ipython installieren, dass eine etwas schickere Oberfläche auf der Kommandozeile bietet:

    ```
    sudo apt-get install ipython
    ```

## LaTeX installieren (optional)

!!! warning Achtung
    Diese Anleitung musst du vermutlich nicht befolgen. Falls du sie befolgen musst, dann erst in der Einheit zu LaTeX.

Da das Dokumentensystem LaTeX sehr groß ist, empfehlen wir diese Anleitung nur zu befolgen, wenn du

1. [https://sharelatex.gwdg.de/](https://sharelatex.gwdg.de/) nicht nutzen kannst, weil du z.B. deine Logindaten noch nicht hast
2. [https://de.overleaf.com/](https://de.overleaf.com/) nicht nutzen möchtest, z.B. aus Datenschutzgründen

Gib in dein Terminal folgendes ein:
```
sudo apt-get install texlive-full
```
Wenn dein Speicherplatz knapp ist, kannst du auch probieren folgendes zu installieren:
```
sudo apt-get install texlive texlive-lang-german texlive-latex-extra 
```
Wir konnten dies aber nicht testen.

Mehr findest du unter: [https://wiki.ubuntuusers.de/TeX_Live/](https://wiki.ubuntuusers.de/TeX_Live/)

# Java installieren

!!! warning Achtung
    Dieser Abschnitt ist erst für die Einheit zu Java relevant.

Führe folgendes in einem Terminal aus:
```
sudo apt-get install openjdk-21-jdk
```

Mehr findest du dazu unter: [https://wiki.ubuntuusers.de/Java/Installation/OpenJDK/](https://wiki.ubuntuusers.de/Java/Installation/OpenJDK/)

Sobald du Java installiert hast, kannst du [unserer Anleitung zum testen folgen](/informatik-vorkurs-anleitungen/java-testen/).