# Orientierungshilfe
Je nachdem womit du arbeitest, musst du ab hier eine andere Anleitung befolgen:

- Wenn du **Windows** benutzt, bietet das Windows Subsystem for Linux (WSL2) die beste Nutzungserfahrung und wird deshalb in der [Windows-Anleitung](../windows/) beschrieben. Bei Problem kannst du auch eine Virtuelle Maschine (VM) mit Linux verwenden (siehe unten).
- Folge der **Linux**-[Anleitung](../linux/), wenn du ein Ubuntu-basiertes Linux (z.B. Ubuntu, Kubuntu, Mint, Lifi-VM, usw.) verwendest. Andere Linux-Varianten funktionieren ähnlich, du kannst dich an der Anleitung orientieren.
- Falls du ein **MacBook** besitzt, kannst du [der Anleitung für MacOS](../macos/) folgen. Bei Problem kannst du auch eine Virtuelle Maschine (VM) mit Linux verwenden (siehe unten).
- Solltest du auf Probleme stoßen, die sich nicht am ersten Tag lösen lassen oder eine der obenstehenden Anleitungen nicht befolgen wollen, kannst du Linux auch als Virtuelle Maschine (VM) installieren. Folge dafür der [Lifi-Anleitung](../lifi/) und anschließend musst du noch der [**Linux**-Anleitung](../linux/) folgen, um die notwendige Software zu installieren.