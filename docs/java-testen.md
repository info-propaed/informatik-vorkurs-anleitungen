# Java testen

!!! warning
    Befolge diese Anleitung erst bei der Java-Einheit.

Um zu testen, ob Java funktioniert, erstelle in VSCodium eine Datei `HalloWelt.java` mit folgendem Inhalt:
```
class HalloWelt {
    public static void main(String[] args) {
        System.out.println("Hallo Welt!"); 
    }
}
```
Es ist wichtig, dass der Dateiname und der Name hinter `class` exakt identisch sind.

Um Java auszuführen, müssen wir immer zwei Schritte unternehmen:

1. den Quelltext in eine maschinenlesbare Form übersetzen lassen (kompilieren). Dafür benutzen wir das Programm `javac`. Es produziert auf `.java` Datein `.class` Dateien.
2. die maschinenlesbare Form ausführen. Dafür benutzen wir das Programm `java`. Es sucht sich die passende `.class` Datei und führt sie aus (interpretiert sie).

Die vollständigen Befehle lauten:

1. `javac HalloWelt.java` (produziert die Datei `HalloWelt.class`)
2. `java halloWelt` (es ist wichtig, dass hier kein `.class` am Ende steht)

Hat alles geklappt, dann sehen wir die Ausgabe
```
Hallo Welt!
```

!!! info
    Tipp: Man kann die Befehle auf der Kommandozeile auch kombinieren, so dass beides in einem Schritt durchgeführt wird: `javac HalloWelt.java && java HalloWelt`.