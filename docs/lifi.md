# Linux als VM

## Lifi oder euer eigenes System
Der erste Frage ist, ob ihr direkt auf eurem Betriebssystem arbeitet oder eine virtuelle Maschine (VM) mit Linux verwendet. Für die virtuelle Maschine spricht, dass sie der Arbeitsumgebung des ersten Semesters entspricht und ein fertiges Linux-System mitbringt. Dagegen sprechen:

- geringere Geschwindigkeit
- höherer Energieverbrauch / geringere Akkulaufzeit
- Trennung zwischen den Daten aus der VM und eurem sonstigen System (obwohl drag+drop funktioniert)
- ermutigt nicht dazu das, was man im Studium lernt auch außerhalb zu verwenden

Deshalb empfehlen wir als ersten Ansatz die Installation direkt auf eurem Betriebssystem und erst bei Problemen [die VM von Linux fürs Institut für Informatik (Lifi)](/informatik-vorkurs-anleitungen/lifi/).

## Einrichtung
Nichts leichter als das. Folge der exzellenten Anleitung unter [https://lifi.pages.gwdg.de/lifi-dokumentation/](https://lifi.pages.gwdg.de/lifi-dokumentation/)!

Darüber hinaus verhält sich Lifi wie Ubuntu und du kannst der [**Linux**-Anleitung](../linux/) folgen.