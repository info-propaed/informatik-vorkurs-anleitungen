# Fernzugriff per SSH

!!! warning
    Befolge diese Anleitung erst bei der Linux/Bash-Einheit.


In den Rechnerräumen der Informatik wird Ubuntu verwendet. Man kann von außen auf diese Rechnersysteme zugreifen (Fernzugriff, engl. Remote Access). Dadurch bekommt man

- Zugriff auf die Dateien auf den Rechnern, auch von zu Hause
- kann unterwegs weiter arbeiten
- hat Zugriff, auch wenn gerade keine Rechner frei sind
- kann Linux-Programme ausführen, auch wenn man kein Linux hat
- kann Dateien hochladen/herunterladen (Nützlich für Hausaufgabenabgabe)

Linux bringt standardmäßig eine Textschnittstelle zur Bedienung mit. Der Fernzugriff, der über das Programm `ssh` erfolgt, baut darauf auf und erlaubt auch (normalerweise) keinen Zugriff auf die grafische Oberfläche. Bei `ssh` agiert ein Computer als Server auf dem man sich über ein Netzwerk von einem anderen Computer (Client) aus anmelden kann. Bei uns ist der Zugriff zusätzlich zu einem Benutzernamen und Passwort durch so genannte Zwei-Faktor-Authentifizierung abgesichert, die zunächst eingerichtet wird.

## Zugriff
Deine Logindaten sind:

1. **Benutzername**/Username: E-Campus-Login bzw. der Teil deiner Email-Adresse vor `@stud.uni-goettingen.de`.
2. **Passwort**: E-Campus-Login Passwort.
3. Ein frisch generierter Zwei-Faktor-Authentifizierungs-Code. Das erklären wir gleich, ließ erst hier weiter.

Wir beschreiben zunächst, wie der Zugriff funktioniert am Beispiel von Linux und Mac.

1. Terminal öffnen
2. `ssh username@serveradresse` eingeben und mit Returntaste bestätigen.
    - dabei musst du `username` durch deinen Benutzernamen ersetzen, z.B. `maxi.mustermann`.
    - `serveradresse` ist für unsere Rechnerräume `shell.informatik.uni-goettingen.de`.
3. Du wirst gebeten ein Passwort einzugeben. Gib dein Passwort ein (es wird beim Tippen nicht angezeigt) und bestätige mit der Returntaste.
4. Zwei-Faktor Verifikations-Code eingeben, mit Returntaste bestätigen.
5. Du bist eingeloggt und kannst nun Textbefehle eingeben (und jeweils mit der Returntaste absenden). Probiere den Befehl `ls`. `ls` zeigt den Inhalt von Ordnern und sollte dir nun eine kurze Liste anzeigen.
6. Du kannst sich wieder ausloggen, indem du `exit` eingibst (und mit der Returntaste bestätigst).

Führe nun zuerst die Ersteinrichtung (siehe unten) durch und probiere dann diesen Ablauf einmal aus. Hinweise zu Windows und wie du Dateien austauschen kannst, findest du ganz am Ende der Anleitung.

## Einrichtung

Die Einrichtung kannst du nur innerhalb der Uni machen. Solltest du das von Außerhalb der Uninetzwerke machen, kannst du [den VPN der Uni einrichten, mit dem dein Datenverkehr ins Uninetzwerk umgeleitet wird](https://docs.gwdg.de/doku.php?id=de:services:network_services:vpn:start).

Für die Einrichtung musst du dich einmalig auf einem bestimmten Einrichtungsserver (`shellinit`) einloggen.
```bash
ssh username@shellinit.informatik.uni-goettingen.de
```

`shellinit` benötigt keinen Zwei-Faktor-Verifikations-Code. Stattdessen erhältst du eine Anleitung, wie du die Zwei-Faktor-Authentifizierung einrichtest. Dazu brauchst du ein Gerät, dass Zwei-Faktor-Authentifizierungs-Codes generieren kann (auch TOTP-Generator genannt). Am einfachsten ist die Einrichtung auf einem Smartphone. Nachdem du eine der untenstehenden Apps installiert hast, kannst du damit den über shellinit angezeigten QR-Code scannen. Danach generiert dir die App immer aktuelle Codes. Folgende Programme/Apps können Codes generieren:

| App inkl. Link | Plattform |
|-----|----------|
| [Aegis Authenticator](https://getaegis.app/)   | Android     |
| [Google Authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2) | Android,iOS |
| [Fast 2FA](https://apps.apple.com/de/app/totp-authenticator-fast-2fa/id1404230533)             | iOS         |
| [oathtool](https://www.nongnu.org/oath-toolkit/)             | Linux       |

Falls du auf Probleme stößt, wird dir am Ende der Ausgabe von shellinit ein Link zu einer ausführlicheren Hilfeseite angezeigt.

## SSH unter Windows

Wenn du unter Windows das Windows Subsystem for Linux (WSL2) eingerichtet hast, dann kannst du darüber wie unter Linux arbeiten.

Anderenfalls gibt es für Windows [das Programm `putty`](https://www.putty.org/), dass eine grafische Oberfläche mitbringt. Sobald du dort unter `Host Name` die Serveradresse eingetragen hast, den Port ggf. auf 22 gesetzt hast und als Verbindungstypen SSH ausgewählt hast, kannst du dich verbinden. Dabei schließt sich das Konfigurationsfenster und es öffnet sich ein Terminalfenster in dem du - anders als bei Linux/Mac - zuerst auch deinen Benutzernamen angeben musst.

## Dateien austauschen per SSH

## Wie schicke ich Dateien vom Fernzugriff-Account zu meinem Rechner / andersrum?

Dafür können wir am einfachsten den **SCP** - Command verwenden. Beachte: Die Dateien werden nicht verschoben, sondern kopiert.
### Schreibweise:
Diese Befehle verwendest du in deinem Terminal, während du **NICHT** im Fernzugriff bist!
#### von deinem Rechner zum CIP-Pool:
```scp /PFAD/ZUR/DATEI USERNAME@shell.informatik.uni-goettingen.de:~/PFAD/ZUM/ZIEL```

#### vom CIP-Pool zu deinem Rechner:

```scp USERNAME@shell.informatik.uni-goettingen.de:~/PFAD/ZUR/DATEI /PFAD/ZUM/ZIEL```

### Beispiel

Eine Datei aus dem Ordner "Test" wird auf den Desktop des CIP-Pool Accounts kopiert.
![Eine Datei aus dem Ordner "Test" wird auf den Desktop des CIP-Pool Accounts kopiert.](https://pad.gwdg.de/uploads/4e7dd91a-a7e5-481e-834c-8fd40058a23d.png)

Wie man sehen kann befindet sich "datei" nun auf dem Desktop.
![Wie man sehen kann befindet sich "datei" nun auf dem Desktop.](https://pad.gwdg.de/uploads/0dbc266e-0877-43e9-ba3b-78f5b390d6c0.png)

Die Datei "datei" wird vom Dekstop des CIP-Pool Accounts in den aktuellen Ordner (.) kopiert.
![Die Datei  "datei" wird vom Dekstop des CIP-Pool Accounts in den aktuellen Ordner (.) kopiert.](https://pad.gwdg.de/uploads/b38b1fbe-3b6c-4690-9c38-a1a2ad4f8bbe.png)

### Alternative: FileZilla

Wer ohne eine grafische Oberfläche nicht Leben kann, kann alternativ auch das kostenlose Programm [FileZilla](https://filezilla-project.org/) verwenden. 
Diese Anleitung wurde von einem Macbook aus erstellt, d.h. Ort von bestimmten Knöpfen/Einstellungen könnte bei dir abweichen!

Wenn du FileZilla öffnest, gehst du zunächst auf die Option "File>Site Manager"
![Wenn du FileZilla öffnest, gehst du zunächst auf die Option "File>Site Manager"](https://pad.gwdg.de/uploads/bafadb57-7640-4fc7-a9fc-6281e13ab08d.png)

Nun füllst du das Feld rechts bei "General" folgendermaßen aus. 
![Nun füllst du das Feld rechts folgendermaßen aus. ](https://pad.gwdg.de/uploads/012ff00e-f07a-46d3-aa25-76e04998fbec.png)
... Und drückst anschließend auf CONNECT.
Du wirst daraufhin:
- nach deinem Verification code gefragt
- danach nach deinem Passwort gefragt
... Und dann sollte die Verbindung aufgebaut sein!

Du kannst nun in dieser Übersicht Dateien per Drag & Drop hin und her kopieren. Links befindet sich die Ordnerstruktur deines Rechners, Rechts die des CIP-Pool Rechners.
Beachte: Bei FileZilla ist es normal, wenn du vor dem kopieren einer Datei jedes Mal nach deinem Verification Code gefragt wirst! Da kann man leider nichts dagegen tun.
![Ordnerstruktur: Links dein Rechner, Rechts der Cip-Pool](https://pad.gwdg.de/uploads/ba4abead-efac-4f3c-9cfd-8fad1fc5c6d5.png)
