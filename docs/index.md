# Einstieg
Mit diesen Anleitungen möchten wir es einfach machen alle nötige Software für [unseren Vorkurs](https://www.uni-goettingen.de/de/646732.html) und das erste Semester einzurichten. Auch wenn wir grundsätzlich nahelegen sich näher mit Linux zu beschäftigen und es dafür auch im Vorkurs und im ersten Semester Gelegenheit gibt, möchten wir euch ermöglichen mit der euch vertrauten Umgebung am Vorkurs teilzunehmen. Zu Studienstart gibt es schon genug Neues.

Da immer nicht alles wie vorgesehen funktioniert, haben wir an mehreren Stellen "Fallbacks". Der größte Fallback ist die Verwendung einer virtuellen Maschine mit Linux, statt einer Installation unter eurem Betriebssystem.

## Erste Schritte

1. [WLAN einrichten (Eduroam)](/informatik-vorkurs-anleitungen/eduroam/)
2. Entscheiden welche(n) Computer du verwenden möchtest.
    - Die Computer aus unseren Rechnerräumen haben die notwendige Software vorinstalliert.
    - Falls du im Semester einen eigenen Laptop verwenden willst, empfehlen wir dir den Vorkurs als Gelegenheit zu nutzen, um alles dafür einzurichten.
    - [Falls du einen Laptop suchst, hat die Fachschaft/Fachgruppe Empfehlungen.](https://fg.informatik.uni-goettingen.de/de/studium/laptopkaufguide.html)
3. Falls du mit einem eigenen Computer arbeitest: [Befolge die jeweilige Anleitung **bis einschließlich Python**](/informatik-vorkurs-anleitungen/orientierungshilfe-betriebssysteme/).
4. Weitere Schritte durchführen, sobald sie im Vorkurs dran kommen.
