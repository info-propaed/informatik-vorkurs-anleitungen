# Windows

Da fast alle Werkzeuge, die im Vorkurs und ersten Semester verwendet werden ursprünglich für Linux (oder ein verwandtes Betriebssystem) entwickelt wurden, ist es der leichteste und angenehmste Weg unter Windows die von Microsoft bereitgestellte Kompatibilitätsschicht Windows Subsystem for Linux (WSL2) zu verwenden. WSL2 integriert ein Linux-System (standardmäßig Ubuntu) in euer Windows. Dabei verhält es sich aber nicht wie eine virtuelle Maschine, sondern eher als könntet ihr Linux-Programme direkt in Windows benutzen. Alle weiteren Dinge installieren wir danach über das Terminal von WSL2.

Deshalb ist der erste Schritt die Installation WSL2 zu installieren. Plant dafür ein bisschen Zeit ein.

## Windows Subsystem for Linux (WSL2)

Optimalerweise muss man nur der Anleitung unter [https://learn.microsoft.com/de-de/windows/wsl/install](https://learn.microsoft.com/de-de/windows/wsl/install) folgen. Beim Testen haben wir hier aber mehrere Probleme gehabt und wollen deshalb auch nochmal Bilder und einige Hinweise bereitstellen. Folge bitte trotzdem der Anleitung.

Ein Tipp vorweg: Unter Windows kann man mit einem Rechtsklick Text in ein Terminal einfügen. Nur immer vorsichtig sein auch alles richtig kopiert zu haben!

![](./img/01_Startmenü.png)
![](./img/02_Powershell_wsl_install.png)
![](./img/03_Powershell_fertig_neustarten.png)
Nach einem Neustart findet sich ein Ubuntu-Terminal im Startmenü:
![](./img/04_Startmenü_WSL.png)
An dieser Stelle hatten wir einige Probleme und mussten erst Updates machen und nochmal neu starten, damit WSL2 gestartet hat. Danach sah es so aus:
![](./img/05_WSL_Username.png)
Die ersten Fehler aus dem Screenshot sind vmtl. unproblematisch und können ignoriert werden. Allerdings tritt hier noch der Fehler auf, dass wir keinen Nutzernamen festlegen können, weil (in irgendeinem Internen Teil von VSCodium) eine Option `--quiet` fehlt. Wir konnten das mit einem kleinen Hack zum Laufen bekommen: Einfach ein `#` hinter den Namen schreiben.

<!-- ![.././img/06_WSL_Login.png]() -->
![](./img/07_WSL_apt_update.png)

Danach konnten wir das Terminal neu starten und waren eingeloggt. Hier lassen sich Befehle ausführen. Von nun an verwenden wir immer das Ubuntu-Terminal. Wie Programme installiert werden können, steht in der Anleitung zu VSCodium.

### Typische Probleme mit WSL
Hier einige Tipps, falls WSL nicht funktioniert:

**Falls kein Nutzername festgelegt werden konnte, kann man dies nachträglich tun, indem man:**

1. in WSL `adduser <username>` ausführt, wobei `<username>` der Nutzername ist, der nur aus Kleinbuchstaben bestehen darf. Danach muss man sich ein Passwort geben (wird beim Tippen nicht angezeigt) und kann einige Fragen beantworten (Antwort kann man leer lassen).
2. in WSL `sudo usermod -a -G sudo <username>` eingeben, damit der Nutzer Adminrechte erhält und z.B. Software installieren kann
3. Alle WSL und VSCodium-Fenster schließen und dann in einer Power Shell (als Administrator ausgeführt) den Befehl `ubuntu config --default-user <username>` eingibt. Danach ist der erstellte Benutzer der Standardbenutzer.

Falls man keine `sudo`-Rechte hat, kann man nach 3. den Benutzer `root` zum Standardbenutzer machen. Dieser Nutzer hat immer Administratorrechte und kann den Befehl unter 2. ausführen. Danach kann man nach 3. wieder zum erstellten Benutzer zurückwechseln.

**Falls Fehler beim Installieren/starten von WSL auftreten:**

- checken ob das Windows Subsystem for Linux in den Windows Features aktiviert ist. Dazu kann man (z.B. über eine normale Powershell) das Windows-Tool `optionalfeatures` starten (findet man auch im Startmenü). Nachdem das Feature aktiviert wurde, muss man den Rechner neu starten.
- falls `wsl --install` nicht weiß, was es installieren soll, kann man das mit `wsl --install -d Ubuntu` explizit angeben
- `wsl --update` ausführen
- wenn spätestens beim Starten von Ubuntu oder wenn man `wsl --status` in einer Power Shell (als Administrator ausgeführt) irgendwo angezeigt wird, dass Virtualisierungsfunktionen fehlen: Hier muss man ins Bios/EFI des Laptops gehen. Dazu muss man beim Start eine bestimmte Taste drücken (findet man im Internet zum Laptop-Hersteller/-Modell). Am besten sucht man das heraus, startet dann Windows neu und sobald der Bildschirm schwarz geworden ist, drückt man wiederholt die entsprechende Taste. Dann muss man im Bios/EFI die Virtualisierungsfunktion finden und aktivieren. Je nach Prozessorhersteller und BIOS/EFI-Hersteller heißt diese Funktionalität leicht anders. Es kann auch sein, dass man mehrere Optionen aktivieren muss.

## VSCodium installieren

Zunächt aktualisieren wir den Paketmanager mit dem Befehl (im Ubuntu-Terminal)
```
sudo apt update
```

Ab hier folgen wir der Anleitung, wie man VSCodium unter Ubuntu (als deb Paket) installiert: [https://vscodium.com/#install-on-debian-ubuntu-deb-package](https://vscodium.com/#install-on-debian-ubuntu-deb-package). Dazu führen wir zuerst folgende Kommandos aus:

![](./img/08_WSL_VSCodium.png)

Als nächstes müssen wir VSCodium noch so einstellen, dass es sich auch aus WSL2 heraus ausführen lässt. Dazu geben wir ins Terminal folgendes ein:
```
DONT_PROMPT_WSL_INSTALL=1 codium .bashrc
```
![](./img/09_WSL_VSCodium_Start.png)
Nun öffnet sich der Editor VSCodium. Vermutlich musst du noch eine Sicherheitswarnung beantworten. In diesem Fall ist es unproblematisch sie abzulehnen. Danach öffnet sich die Konfigurationsdatei `.bashrc` in der wir folgenden Text ganz am Ende einfügen:
```
export DONT_PROMPT_WSL_INSTALL=1
```
![](./img/10_WSL_VSCodium_Config.png)
Nach einem Neustart des Terminals sollte sich VSCodium einfach als `codium` bzw. `codium <ordner>` oder `codium <datei>` starten lassen, wobei `<ordner>` und `<datei>` einen Pfad zu einem Verzeichnis oder einer Datei bezeichnen (ausgehend von dem Verzeichnis, in dem sich das Terminal gerade befinden).

Tipp: Falls dir der Editor zu klein eingezeigt wird, kannst du VSCodium mit `STRG` + `+` vergrößern bzw. `STRG` + `-` verkleinern.

Falls dieser Installationsweg nicht funktioniert, kannst der ersten Antwort in diesem Beitrag folgen: https://stackoverflow.com/questions/72011852/how-to-setup-windows-subsystem-linux-wsl-2-with-vscodium-on-windows-10. Falls du VSCodium nicht innerhalb von WSL, sondern Windows installieren willst, findest du eine Installationsdatei hier: [https://github.com/VSCodium/vscodium/releases](https://github.com/VSCodium/vscodium/releases). Benutze am besten die `VSCodium-x64-<version>.msi`, die du vmtl. erst einblenden lassen musst. Und eine Integration mit WSL2 benötigt [etwas getrickse](https://stackoverflow.com/questions/72011852/how-to-setup-windows-subsystem-linux-wsl-2-with-vscodium-on-windows-10).

Ab hier verhält sich die Anleitung identisch zu Linux/Ubuntu.

## Python installieren

Hura! Du musst nichts tun, denn python ist in Ubuntu schon vorinstalliert.

Mehr findest du unter: [https://wiki.ubuntuusers.de/Python/](https://wiki.ubuntuusers.de/Python/)

!!! note "Tipp"
    Um Python auf der Kommandozeile komfortabler zu benutzen, kannst du zusätzlich ipython installieren, dass eine etwas schickere Oberfläche auf der Kommandozeile bietet:

    ```
    sudo apt-get install ipython
    ```

## LaTeX installieren (optional)

!!! warning Achtung
    Diese Anleitung musst du vermutlich nicht befolgen. Falls du sie befolgen musst, dann erst in der Einheit zu LaTeX.

Da das Dokumentensystem LaTeX sehr groß ist, empfehlen wir diese Anleitung nur zu befolgen, wenn du

1. [https://sharelatex.gwdg.de/](https://sharelatex.gwdg.de/) nicht nutzen kannst, weil du z.B. deine Logindaten noch nicht hast
2. [https://de.overleaf.com/](https://de.overleaf.com/) nicht nutzen möchtest, z.B. aus Datenschutzgründen

Gib in dein Terminal folgendes ein:
```
sudo apt-get install texlive-full
```
Wenn dein Speicherplatz knapp ist, kannst du auch probieren folgendes zu installieren:
```
sudo apt-get install texlive texlive-lang-german texlive-latex-extra 
```
Wir konnten dies aber nicht testen.

Mehr findest du unter: [https://wiki.ubuntuusers.de/TeX_Live/](https://wiki.ubuntuusers.de/TeX_Live/)

# Java installieren

!!! warning Achtung
    Dieser Abschnitt ist erst für die Einheit zu Java relevant.

Führe folgendes in einem Terminal aus:
```
sudo apt-get install openjdk-21-jdk
```

Mehr findest du dazu unter: [https://wiki.ubuntuusers.de/Java/Installation/OpenJDK/](https://wiki.ubuntuusers.de/Java/Installation/OpenJDK/)

Sobald du Java installiert hast, kannst du [unserer Anleitung zum testen folgen](/informatik-vorkurs-anleitungen/java-testen/).
