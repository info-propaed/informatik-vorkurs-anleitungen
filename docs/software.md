# Über die verwendete Software
## VSCodium
VSCodium ist eine quelloffene Version des beliebten Code-Editors Visual Studio Code (VS Code). Im Gegensatz zu VS Code ist VSCodium vollständig frei von proprietären Komponenten und bietet eine transparente, community-basierte Entwicklungsumgebung. Das bedeutet, dass keine proprietären Microsoft-Dienste oder Telemetrie in die Software integriert sind. VSCodium bewahrt alle Funktionen und Erweiterbarkeiten von VS Code und ermöglicht Entwicklern eine flexible und anpassbare Arbeitsumgebung für verschiedene Programmiersprachen. Dank der Offenheit des Projekts können Benutzer vollständig die Kontrolle über ihre Entwicklungsumgebung behalten und individuelle Anpassungen vornehmen.

VSCodium ist die einfachste Möglichkeit für euch beim Programmieren lernen einfach loszulegen, ohne dass ihr euch den Kopf zerbrechen müsst oder umständlich eingewöhnen müsst.

## LaTeX
LaTeX ist ein professionelles Textsatzsystem, das sich besonders für wissenschaftliche Dokumente, technische Berichte und mathematische Formeln eignet. Im Gegensatz zu herkömmlichen Textverarbeitungsprogrammen legt LaTeX den Fokus auf die logische Struktur des Dokuments. Es verwendet Markup-Befehle, um Formatierung und Layout zu definieren. Dadurch ermöglicht es eine präzise und konsistente Gestaltung von Dokumenten. LaTeX ist weit verbreitet in der akademischen Welt und bietet eine effiziente Methode zur Erstellung anspruchsvoller Dokumente, insbesondere solcher mit komplexen mathematischen Inhalten.

LaTeX wird in der Informatik in der Regel für Abschlussarbeiten und Projektberichte vorausgesetzt.

## Python
Python ist eine vielseitige, interpretierte Programmiersprache, die sich durch eine klare und leicht verständliche Syntax auszeichnet. Sie eignet sich hervorragend für Anfänger und erfahrene Entwickler gleichermaßen. Python findet breite Anwendung in der Softwareentwicklung, Datenanalyse, künstlichen Intelligenz, Webentwicklung und mehr. Dank seiner umfangreichen Bibliotheksammlung ermöglicht Python effiziente Lösungen für eine Vielzahl von Aufgaben. Die aktive Community und die große Nutzerbasis tragen dazu bei, Python als eine der beliebtesten Programmiersprachen weltweit zu etablieren.

Python wird bei uns ab dem dritten Semester in einigen Lehrveranstaltungen eingesetzt.

## Homebrew
Homebrew ist ein Paketmanager für macOS, der es Benutzern ermöglicht, schnell und einfach Software und Bibliotheken zu installieren, die auf dem Betriebssystem nicht standardmäßig verfügbar sind. Es automatisiert den Prozess der Installation, Aktualisierung und Deinstallation von Anwendungen über die Kommandozeile. Homebrew bietet eine umfangreiche Sammlung von Paketen aus Open-Source-Projekten, was die Bereitstellung von Entwicklertools und Anwendungen erheblich vereinfacht. Mit einer aktiven Community und regelmäßigen Updates ist Homebrew eine äußerst nützliche Ressource für macOS-Benutzer.

## WSL2
WSL2 (Windows Subsystem for Linux 2) ist eine leistungsstarke Funktion von Windows, die es Benutzern ermöglicht, eine vollständige Linux-Umgebung direkt auf einem Windows-Betriebssystem auszuführen. Im Gegensatz zu seinem Vorgänger bietet WSL2 eine echte Linux-Kernel-Virtualisierung, was zu einer deutlich verbesserten Leistung und Kompatibilität führt. Dies ermöglicht die nahtlose Integration von Linux-Tools und Anwendungen in die Windows-Umgebung, wodurch Entwicklern eine vielseitige Entwicklungsumgebung geboten wird, die das Beste aus beiden Welten vereint.

WSL2 ist für Windows-Nutzende die einfachste Möglichkeit viele der Vorteile von Linux zu nutzen.

## Java
Java ist eine objektorientierte Programmiersprache, die sich durch ihre Plattformunabhängigkeit auszeichnet. Entwickelt von Sun Microsystems, ermöglicht Java die Erstellung von Anwendungen, die auf verschiedenen Betriebssystemen ausgeführt werden können. Es ist besonders bekannt für seine Robustheit, Sicherheit und Portabilität. Java-Code wird in sogenannten Bytecode kompiliert, der von einer Java Virtual Machine (JVM) ausgeführt wird. Dadurch können Java-Anwendungen auf unterschiedlichen Systemen laufen, ohne erneut kompiliert werden zu müssen. Aufgrund seiner Vielseitigkeit ist Java weit verbreitet und wird in einer Vielzahl von Anwendungsgebieten eingesetzt, von mobilen Apps bis hin zu Enterprise-Software.

Java wird bei uns in den ersten Semestern viel eingesetzt.

## Disclaimer
Die Texte auf dieser Unterseite wurden mit ChatGPT generiert und händisch nachbearbeitet.
